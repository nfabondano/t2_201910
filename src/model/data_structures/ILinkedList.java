package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T> Clase g�nerica
 */
public interface ILinkedList<T> extends Iterable<T> {

	/**
	 * Agrega un objeto al comienzo de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 */
	public void add(T pNew);

	/**
	 * Agrega un objeto al final de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 */
	public void addAtEnd(T pNew);

	/**
	 * Agrega un objeto en una posici�n determinada de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 * @param posK Posici�n del nuevo elemento. posK >= 0.
	 */
	public void addAtK(int posK, T pNew);

	/**
	 * Retorna el elemento dado por parametro
	 * @param pPos Posci�n del elemento buscado. pPos >= 0.
	 * @return El elemento buscado.
	 */
	public T getElement( int pPos);

	/**
	 * Retorna el elemento actual de la lista encadenada.
	 * @return El elemento actual.
	 */
	public T getCurrentElement();

	/**
	 * Retorna el tama�o de la lista encadenada
	 * @return El tama�o de la lista encadenada.
	 */
	public Integer getSize();

	/**
	 * Elimina el �ltimo elemento de la lista.
	 * @return El elemento eliminado.
	 */
	public T delete();

	/**
	 * Elimina un elemento de una poscisi�n determinada y retorna lo eliminado.
	 * @param posK Posici�n del elemento a eliminar. posK >= 0.
	 * @return El elemento eliminado.
	 */
	public T deleteAtK(int posK);

	/**
	 * Cambia el actual al siguiente, y retorna este mismo.
	 * @return El siguiente de la lista.
	 */
	public T next();

	/**
	 * Cambia el actual al previo, y retorna este mismo.
	 * @return El previo de la lista.
	 */
	public T previous();
}
