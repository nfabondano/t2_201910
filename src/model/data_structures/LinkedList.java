package model.data_structures;

import java.util.Iterator;

/**
 * Clase que representa una lista enlazada gen�rica.
 * @author Nicol�s Abondano. 201812467	
 * Implementa ILinkedList
 * @param <T> Clase g�nerica.
 */
public class LinkedList<T> implements ILinkedList<T>{

	/**
	 * Nodo actual de la lista.
	 */
	Node actual;

	/**
	 * Primer nodo de la lista.
	 */
	Node primero;

	/**
	 * �litmo nodo de la lista.
	 */
	Node ultimo;

	/**
	 * Tama�o de la lista
	 */
	int size;

	/**
	 * Constructor de la lista.
	 */
	public LinkedList(){
		primero = null;
		ultimo = null;
		actual = null;
		size = 0;
	}
	
	/**

	 * Clase nodo, se usa para guardar los datos de la lista
	 * @author Nicol�s Abondano. 201812467
	 *
	 */
	class Node{

		/**
		 * El siguiente nodo.
		 */
		Node next;

		/**
		 * El anterior nodo.
		 */
		Node previous;

		/**
		 * El dato guardado en el nodo.
		 */
		T dato;

		/**
		 * Constructor del nodo,
		 */
		public Node(){ 
			next=null; 
			previous = null;
			dato = null;
		}
	}

	/**
	 * Retorna el tama�o de la lista encadenada
	 * @return El tama�o de la lista encadenada.
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Agrega un objeto al comienzo de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 */
	public void add(T pNew){
		Node nuevo = new Node();
		nuevo.dato = pNew;
		if(primero == null) {
			primero = nuevo;
			actual = primero;
		}
		else {
			nuevo.next = primero;
			primero.previous = nuevo;
			primero = nuevo;
		}
		if(ultimo == null)
			ultimo = nuevo;
		actual = primero;
		size++;
	}

	/**
	 * Agrega un objeto al final de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 */
	public void addAtEnd(T pNew) {
		Node nuevo = new Node();
		nuevo.dato = pNew;
		if(primero == null) {
			primero = nuevo;
			actual = primero;
		}
		if(ultimo == null)
			ultimo = nuevo;
		else {
			nuevo.previous = ultimo;
			ultimo.next = nuevo;
			ultimo = nuevo;
		}
		size++;
	}

	/**
	 * Agrega un objeto en una posici�n determinada de la lista encadenada.
	 * @param pNew Nuevo elemento. pNew != null.
	 * @param posK Posici�n del nuevo elemento. posK >= 0 && posK <= size.
	 */ 
	public void addAtK(int posK, T pNew) {
		if(posK == 0)
			add(pNew);
		if(posK == size)
			addAtEnd(pNew);
		Node act = primero;
		Node nuevo = new Node();
		nuevo.dato = pNew;
		for(int i=0; i<posK; i++) {
			act = act.next;
		}
		act.previous.next = nuevo;
		nuevo.previous = act.previous;
		nuevo.next = act;
		act.previous = nuevo;

		size++;
	}

	/**
	 * Retorna el elemento dado por parametro
	 * @param pPos Posci�n del elemento buscado. pPos >= 0 && pPos < size.
	 * @return El elemento buscado.
	 */
	public T getElement( int pPos) {
		Node act = primero;
		for (int i = 0; i < pPos; i++) {
			act = act.next;
		}
		return act.dato;
	}

	/**
	 * Retorna el elemento actual de la lista encadenada.
	 * @return El elemento actual.
	 */
	public T getCurrentElement() {
		return actual.dato;
	}

	/**
	 * Cambia el actual al siguiente, y retorna este mismo.
	 * @return El siguiente de la lista.
	 */
	public T next() {
		actual = actual.next;
		return actual.dato;
	}

	/**
	 * Cambia el actual al previo, y retorna este mismo.
	 * @return El previo de la lista.
	 */
	public T previous() {
		actual = actual.previous;
		return actual.dato;
	}

	/**
	 * Elimina el �ltimo elemento de la lista.
	 * @return El elemento eliminado.
	 */
	public T delete() {
		T borrado = ultimo.dato;
		ultimo = ultimo.previous;
		ultimo.next = null;
		size--;
		return borrado;
	}

	/**
	 * Elimina un elemento de una poscisi�n determinada y retorna lo eliminado.
	 * @param posK Posici�n del elemento a eliminar. posK >= 0 && posK < size.
	 * @return El elemento eliminado.
	 */
	public T deleteAtK(int posK) {
		T eliminado = null;
		Node act = primero;
		if(posK == 0) {
			eliminado = primero.dato;
			primero  = primero.next;
			primero.previous = null;
		}
		else{
			act = primero;
			for (int i = 0; i < posK; i++) {
				act = act.next;
			}
			eliminado = act.dato;
			act.previous.next = act.next;
			act.next.previous = act.previous;
		}
		size--;
		return eliminado;
	}

	/**
	 * Crea un objeto de tipo iterador de la lista.
	 */
	@Override
	public Iterator<T> iterator() {
		return new IteratorList (primero);
	}

	/**
	 * Clase que implementa iterador para utilizar con la lista.
	 * @author Nicol�s Abondano 201812467
	 *
	 */
	private class IteratorList implements Iterator<T> {

		/**
		 * Pr�ximo nodo a recorrer por el iterador.
		 */
		private Node proximo;

		/**
		 * Constructor de la clase lista iterador.
		 */
		public IteratorList(Node primero) {
			proximo = primero;
		}

		/**
		 * Retorna si hay un proximo nodo a recorrer
		 * @return Si hay siguiente nodo a recorrer
		 */
		public boolean hasNext() {
			if(proximo == null)
				return false;
			return true;
		}

		/**
		 * Retorna el proximo nodo a recorrer
		 * @return el proximo nodo a recorrer
		 */
		public T next() {
			T actual = proximo.dato;
			proximo = proximo.next;
			return actual;
		}

		/**
		 * M�todo no implementado
		 */
		@Override
		public void remove() {

		}
	}
}
