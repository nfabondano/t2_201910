package model.logic;

import java.io.*;
import java.util.Iterator;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;
import com.opencsv.CSVReader;

/**
 * Clase que maneja las VOMovingViolation.
 * @author Nicol�s Abondano
 *
 */
public class MovingViolationsManager implements IMovingViolationsManager {

	/**
	 * Lista de VOMovingViolations
	 */
	private LinkedList<VOMovingViolations> lista;

	/**
	 * M�todo que carga las violaciones del archivo csv
	 */
	public void loadMovingViolations(String movingViolationsFile){
		try {
			lista = new LinkedList<VOMovingViolations>();
			File m = new File(movingViolationsFile);
			FileReader nm = new FileReader(m);
			CSVReader reader = new CSVReader(nm);
			String nextLine[];
			nextLine = reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				lista.addAtEnd(new VOMovingViolations(Integer.parseInt(nextLine[0]),nextLine[2], nextLine[13], 
						Integer.parseInt(nextLine[9]), nextLine[12], nextLine[14], nextLine[16] ));
			}
			reader.close();
		}
		catch (Exception e) {

		}
	}

	/**
	 * M�todo que retorna las violaciones por codigo.
	 * @param violationCode El c�digo de violaci�n hecho.
	 * @return Lista enlacada con las violaciones hechas por ese c�digo.
	 */
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList <VOMovingViolations> listaCodigo = new LinkedList<VOMovingViolations>();
		for (VOMovingViolations actual : lista)
			if(actual.getViolationCode().equals(violationCode))
				listaCodigo.add(actual);
		return listaCodigo;
	}

	/**
	 * M�todo que retorna las violaciones por accidente.
	 * @param violationCode El accidente de violaci�n hecho.
	 * @return Lista enlacada con las violaciones de ese tipo de accidente.
	 */
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList <VOMovingViolations> listaAccidente = new LinkedList<VOMovingViolations>();
		for (VOMovingViolations actual : lista)
			if(actual.getAccidentIndicator().equals(accidentIndicator))
				listaAccidente.add(actual);
		return listaAccidente;
	}	


}
