package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	/**
	 * ObjectID del VOMovingViolations.
	 */
	private int objectID;

	/**
	 * Location del VOMovingViolations.
	 */
	private String location;

	/**
	 * TicketIssueDate del VOMovingViolations.
	 */
	private String ticketIssueDate;

	/**
	 * TotalPaid del VOMovingViolations.
	 */
	private int totalPaid;

	/**
	 * AccidentIndicator del VOMovingViolations.
	 */
	private String accidentIndicator;

	/**
	 * ViolationCode del VOMovingViolations.
	 */
	private String violationCode;

	/**
	 * ViolationDescription del VOMovingViolations.
	 */
	private String violationDescription;

	/**
	 * Constructor del VOMovingViolations
	 * @param pObjectID ObjectID del VOMovingViolations.
	 * @param pLocation Location del VOMovingViolations.
	 * @param pTicket TicketIssueDate del VOMovingViolations.
	 * @param pPaid TotalPaid del VOMovingViolations.
	 * @param pIndicator AccidentIndicator del VOMovingViolations.
	 * @param pCode ViolationCode del VOMovingViolations.
	 * @param pDescription ViolationDescription del VOMovingViolations.
	 */
	public VOMovingViolations( int pObjectID, String pLocation, String pTicket, int pPaid, String pIndicator, String pCode, String pDescription){
		objectID = pObjectID;
		location = pLocation;
		ticketIssueDate = pTicket;
		totalPaid = pPaid;
		accidentIndicator = pIndicator;
		violationCode = pCode;
		violationDescription = pDescription;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectID;
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}

	/**
	 * @return violationCode - Devuelve el c�digo de violaci�n.
	 */
	public String getViolationCode() {
		return violationCode;
	}
}
