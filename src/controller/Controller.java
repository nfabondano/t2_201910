package controller;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

/**
 * Clase que controla al manager.
 * @author Nicol�s Abondano 201812467
 *
 */
public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();

	/**
	 * M�todo que carga las violaciones del archivo csv
	 */
	public static void loadMovingViolations() {
		manager.loadMovingViolations("./data/Moving_Violations_Issued_In_January_2018.csv");
	}

	/**
	 * M�todo que retorna las violaciones por codigo.
	 * @param violationCode El c�digo de violaci�n hecho.
	 * @return Lista enlacada con las violaciones hechas por ese c�digo.
	 */
	public static LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		return manager.getMovingViolationsByViolationCode(violationCode);
	}

	/**
	 * M�todo que retorna las violaciones por accidente.
	 * @param violationCode El accidente de violaci�n hecho.
	 * @return Lista enlacada con las violaciones de ese tipo de accidente.
	 */
	public static LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		return manager.getMovingViolationsByAccident(accidentIndicator);
	}
}
