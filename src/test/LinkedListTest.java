package test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.data_structures.LinkedList;
/**
 * Clase usada para verificar si la lista enlazada esta implementada correctamente.
 * @author Nicol�s Abondano 201812467
 *
 */
class LinkedListTest {

	/**
	 * Clase sobre la que se realizan las pruebas.
	 */
	LinkedList<String> listaString;

	/**
	 * Escenario 1: Crea una lista vac�a
	 */
	public void setupEscenario1() {
		listaString = new LinkedList<String>();
	}

	/**
	 * Escenario 2: Crea una lista con 6 objetos tipo string.
	 */
	public void setupEscenario2() {
		listaString = new LinkedList<String>();

		listaString.add("Sexto");
		listaString.add("Quinto");
		listaString.add("Cuarto");
		listaString.add("Tercero");
		listaString.add("Segundo");
		listaString.add("Primero");
	}

	/**
	 * Prueba 1: Verifica que add este bien implementado.
	 */
	@Test
	public void testAdd() {
		setupEscenario1();
		listaString.add("Primero");
		assertTrue("No se a�adi� correctamente", listaString.getElement(0).equals("Primero") && listaString.getSize() == 1);
	}

	/**
	 * Prueba 2: Verifica que getElement este bien implementado.
	 */
	@Test
	public void testGetElement() {
		setupEscenario2();
		String t = listaString.getElement(3);
		assertTrue("No se obtuvo el objeto requerido", t.equals("Cuarto"));
	}

	/**
	 * Prueba 3: Verifica que getSize este bien implementado.
	 */
	@Test
	public void testGetSize() {
		setupEscenario2();
		assertTrue("El tama�o no es correcto", listaString.getSize() == 6);
	}

	/**
	 * Prueba 4: Verifica que addAtEnd este bien implementado.
	 */
	@Test
	public void testAddAtEnd() {
		setupEscenario2();
		listaString.addAtEnd("S�ptimo");
		String t = listaString.getElement(listaString.getSize()-1);
		assertTrue("No se a�adi� correctamente", t.equals("S�ptimo"));
	}

	/**
	 * Prueba 5: Verifica que addAtK este bien implementado.
	 */
	@Test
	public void testAddAtK() {
		setupEscenario2();
		listaString.addAtK(4, "Pi");
		String t = listaString.getElement(4);
		assertTrue("No se a�adi� correctamente", t.equals("Pi"));
	}

	/**
	 * Prueba 6: Verifica que getCurrentElement este bien implementado.
	 */
	@Test
	public void testGetCurrentElement() {
		setupEscenario2();
		String t = listaString.getCurrentElement();
		assertTrue("No se obtuvo el objeto requerido", t.equals("Primero"));
	}

	/**
	 * Prueba 7: Verifica que next este bien implementado.
	 */
	@Test
	public void testNext() {
		setupEscenario2();
		listaString.next();
		String t = listaString.getCurrentElement();
		assertTrue("No se obtuvo el objeto requerido", t.equals("Segundo"));
	} 

	/**
	 * Prueba 8: Verifica que previous este bien implementado.
	 */
	@Test
	public void testPrevious() {
		setupEscenario2();
		listaString.next();
		listaString.next();
		listaString.next();
		listaString.previous();
		String t = listaString.getCurrentElement();
		assertTrue("No se obtuvo el objeto requerido", t.equals("Tercero"));
	} 

	/**
	 * Prueba 9: Verifica que delete este bien implementado.
	 */
	@Test
	public void testDelete() {
		setupEscenario2();
		String t = listaString.delete();
		assertTrue("No se elimino el objeto", listaString.getSize() == 5);
		assertTrue("No se elimino el objeto requerido", t.equals("Sexto"));
	}

	/**
	 * Prueba 10: Verifica que deleteAtK este bien implementado.
	 */
	@Test
	public void testDeleteAtK() {
		setupEscenario2();
		String t = listaString.deleteAtK(3);
		assertTrue("No se elimino el objeto", listaString.getSize() == 5);
		assertTrue("No se elimino el objeto requerido", t.equals("Cuarto"));
	}

}
