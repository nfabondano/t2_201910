package api;

import model.data_structures.LinkedList;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	void loadMovingViolations(String movingViolationsFile);

	/**
	 * M�todo que retorna las violaciones por codigo.
	 * @param violationCode El c�digo de violaci�n hecho.
	 * @return Lista enlacada con las violaciones hechas por ese c�digo.
	 */
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode);

	/**
	 * M�todo que retorna las violaciones por accidente.
	 * @param violationCode El accidente de violaci�n hecho.
	 * @return Lista enlacada con las violaciones de ese tipo de accidente.
	 */
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator);


}
